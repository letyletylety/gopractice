// package name of the program
package main

// preprocessor command
import (
	"fmt"
	"log"
	"net/http"
)

// const
const (
	// ConnHost ..
	ConnHost = "localhost"
	// ConnPort ..
	ConnPort = "8080"
)

func helloWorld(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}

func main() {
	// helloWord가 실행되고 나서, / 패턴의 HTTP URL에 접근할 때 마다 http.ResponseWriter, *http.Request 를 인자로 전달
	http.HandleFunc("/", helloWorld)
	// http 요청을 serve 하고 들어오는 연결을 각각의 Goroutine 에서 관리.
	// server address 와 handler를 받음
	// handler에 nil이 들어가면 DefaultServeMux 를 핸들러로 사용
	err := http.ListenAndServe(ConnHost+":"+ConnPort, nil)

	// 에러가 발생하면 로그 찍고 탈출
	if err != nil {
		log.Fatal("error starting http server : ", err)
		return
	}
}
